const mongoose = require('mongoose')

mongoose.connect('mongodb://admin:password@localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection erro:'))
db.once('open', function () {
  console.log('connect')
})
